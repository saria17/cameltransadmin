<%-- 
    Document   : admins
    Created on : May 31, 2016, 9:45:16 PM
    Author     : Sariya
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">

            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a data-toggle="model" class="btn btn-info add-admin"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <span style="color: red"> ${msg} <br/>

                </span>
                <span style="color: #006400"> ${smsg}</span>

                <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                    <thead>
                        <tr>
                            <th> ID </th>
                            <th> Name</th>

                            <th> Edit </th>
                            <th> Delete </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${adminList}" var="ad">
                            <tr>
                                <td> <c:out value="${ad.getId()}"/> </td>
                                <td><c:out value="${ad.getName()}"/>  </td>

                                <td>
                                    <a data-toggle="model" class="btn btn-info edit-admin" href="" data-id="${ad.getId()}"  data-name="${ad.getName()}" >
                                        <i class="glyphicon glyphicon-edit icon-white"></i>
                                        Edit
                                    </a>
                                </td>
                                <td>
                                    <a data-toggle="model" class="btn btn-danger delete-row" data-id="${ad.getId()}"> Delete
                                        <i class="fa fa-minus"></i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="modal fade" id="addadmin" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Adding</h3>
                        </div>
                        <div class="modal-body">
                            <f:form action="RegisterAdmin" commandName="admin" method="post" class="form1">
                                <div class="form-group">
                                    <label class="control-label"  for="name"> Name</label>
                                    <f:input type="text" path="name" id="name" class="form-control"/> </div>
                                    <f:errors path="name" cssClass="error"/>
                                <div class="form-group">
                                    <label class="control-label" for="password">Password</label>
                                    <f:input type="password" path="password" id="password" class="form-control"/> 
                                    <f:errors path="password" cssClass="error"/>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Confirm Password</label>
                                    <f:input type="password" path="rpassword" id="rpassword" class="form-control"/> 
                                </div>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Add"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </f:form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="deleterow" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Delete</h3>
                        </div>
                        <div class="modal-body">
                            <div>    <p>Are you sure to permanently delete row <b id="idvalue"></b> ?</p> </div>
                            <form action="DeleteAdmin" method="post" >
                                <input type="hidden" name="Id" id="id" class="form-control"/>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Delete"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="editadmin" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Editing</h3>
                        </div>
                        <div class="modal-body">
                            <f:form action="EditAdmin" commandName="admin" method="post" class="editAdmin" >
                                 <f:hidden  path="Id" id="id" class="form-control"/> 
                                <div class="form-group">
                                    <label class="control-label"  for="name"> Name</label>
                                    <f:input type="text" path="name" id="name" class="form-control"/> </div>
                                    <f:errors path="name" cssClass="error"/>
                                <div class="form-group">
                                    <label class="control-label" for="password">Password</label>
                                    <f:input type="password" path="password" id="editpassword" class="form-control"/> 
                                    <f:errors path="password" cssClass="error"/>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Confirm Password</label>
                                    <f:input type="password" path="rpassword" id="rpassword" class="form-control"/> 
                                </div>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Save"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </f:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
