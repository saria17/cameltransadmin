
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">

            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                               <a data-toggle="model" class="btn btn-info add-adr"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <span style="color: red"> ${msg}</span>
                <span style="color: #006400"> ${smsg}</span>
                <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                    <thead>
                        <tr>
                            <th> ID </th>
                            <th> Number </th>
                            <th> Description </th>

                            <th> Edit </th>
                            <th> Delete </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${adrList}" var="adr">
                            <tr>
                                <td> <c:out value="${adr.getId()}"/> </td>
                                <td><c:out value="${adr.getNumber()}"/>  </td>

                                <td class="center"><c:out value="${adr.getDescription()}"/> </td>
                                <td>
                                    <a class="edit" href="javascript:;"> Edit </a>
                                </td>
                                <td>
                                      <a data-toggle="model" class="btn btn-danger delete-row" data-id="${adr.getId()}"> Delete
                                        <i class="fa fa-minus"></i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                      <div class="modal fade" id="addadr" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Adding</h3>
                        </div>
                        <div class="modal-body">
                            <form action="AddAdr" method="post" id="adr">
                                <div class="form-group">
                                    <label class="control-label"  for="number"> Number</label>
                                    <input type="number" name="number" id="number" class="form-control"/> </div>

                                <div class="form-group">
                                    <label class="control-label" for="description">Description</label>
                                    <input type="text" name="description" id="description" class="form-control"/> 
                                </div>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Add"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="deleterow" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Delete</h3>
                        </div>
                        <div class="modal-body">
                            <div>    <p>Are you sure to permanently delete row <b id="idvalue"></b> ?</p> </div>
                            <form action="DeleteADR" method="post" >
                                <input type="hidden" name="Id" id="id" class="form-control"/>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Delete"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
