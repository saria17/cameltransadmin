<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">

            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a data-toggle="model" class="btn btn-info add-city"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <span style="color: red"> ${msg}</span>
                <f:errors path="citym"/>
                <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                    <thead>
                        <tr>
                            <th> ID </th>
                            <th> Name </th>
                            <th> Country </th>

                            <th> Edit </th>
                            <th> Delete </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${cityList}" var="c">
                            <tr>
                                <td>  <c:out value="${c.getId()}"/> </td>
                                <td>  <c:out value="${c.getName()}"/>  </td>
                                <td>  <c:out value="${c.getCountry().getName()}"/>  </td>

                                <td>
                                    <a data-toggle="model" class="btn btn-info edit-row" href="" data-id="${c.getId()}"  data-name="${c.getName()}" data-country="${c.getCountry().getName()}">
                                        <i class="glyphicon glyphicon-edit icon-white"></i>
                                        Edit
                                    </a>
                                </td>
                                <td>
                                    <a data-toggle="model" class="btn btn-danger delete-row" data-id="${c.getId()}"> Delete
                                        <i class="fa fa-minus"></i>
                                    </a>
                                </td>
                            </tr>

                        </c:forEach>

                    </tbody>
                </table>
                <div class="modal fade" id="addcity" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Adding</h3>
                        </div>
                        <div class="modal-body ">
                            <form action="AddCity" method="post" class="form1">
                                <div class="form-group">
                                    <label class="control-label"  for="name">City Name</label>
                                    <input type="text" name="name" id="name" class="form-control"/> </div>

                                <div class="form-group">
                                    <label class="control-label" for="country">Country</label>
                                    <select class="form-control" name="country.Id" id="country">
                                        <c:forEach items="${countries}" var="c">
                                            <option value="${c.getId()}"><c:out value="${c.getName()}"/></option>
                                        </c:forEach>
                                    </select></div>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Add"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="deleterow" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Delete</h3>
                        </div>
                        <div class="modal-body">
                            <div>    <p>Are you sure to permanently delete row <b id="idvalue"></b> ?</p> </div>
                            <form action="DeleteCity" method="post" >
                                <input type="hidden" name="Id" id="id" class="form-control"/>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Delete"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="editrow" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Editing</h3>
                        </div>
                        <div class="modal-body edit">
                            <form action="EditCity" method="post" class="form1">
                                <input type="hidden" id="id" name="Id">
                                <div class="form-group">
                                    <label class="control-label"  for="name">City Name</label>
                                    <input type="text" name="name" id="name" class="form-control"/> </div>

                                <div class="form-group">
                                    <label class="control-label" for="country">Country</label>
                                    <select class="form-control" name="country.Id" id="country">
                                        <c:forEach items="${countries}" var="c">
                                            <option value="${c.getId()}"><c:out value="${c.getName()}"/></option>
                                        </c:forEach>
                                    </select></div>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Save"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>


