
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">

           <div class="portlet-body">
                <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> User</th>
                            <th> Start date loading </th>
                            <th> End date loading </th>
                            <th> Loading country </th>
                            <th> Loading city </th>
                            <th> Loading region </th>
                            <th> Unloading country </th>
                            <th> Unloading city </th>
                            <th> Unloading region </th>
                            <th> Width </th>
                            <th> Length</th>
                            <th> Height</th>
                            <th> Min weight </th>
                            <th> Max weight</th>
                            <th> Min volume</th>
                            <th> Max volume</th>
                            <th> Body type</th>
                            <th> Number of autos</th>
                            <th> Usage amount</th>
                            <th> Payment moment</th>
                            <th> Payment method</th>
                            <th> Price</th>
                            <th> Currency</th>
                            <th> Calculate Unit</th>
                            <th> Prepayment ratio</th>
                            <th> Notes</th>
                            <th> Updated Date</th>
                            <th> Created Date</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${freightList}" var="f">
                        <tr>
                            <td> <c:out value="${f.getName()}"/> </td>
                            <td><c:out value="${f.getUser().getFullName()}"/></td>
                            <td> <c:out value="${f.getStartDateLoading()}"/> </td>
                            <td class="center"> <c:out value="${f.getEndDateLoading()}"/> </td>
                            <td> <c:out value="${f.getLoadingCountry().getName()}"/></td>
                            <td> <c:out value="${f.getLoadingCity().getName()}"/> </td>
                            <td> <c:out value="${f.getLoadingRegion().getName()}"/> </td>
                            <td> <c:out value="${f.getUnLoadingCountry().getName()}"/></td>
                            <td> <c:out value="${f.getUnLoadingCity().getName()}"/> </td>
                            <td> <c:out value="${f.getUnLoadingRegion().getName()}"/> </td>
                            <td> <c:out value="${f.getWidth()}"/> </td>
                            <td> <c:out value="${f.getLength()}"/> </td>
                            <td> <c:out value="${f.getHeight()}"/> </td>
                            <td> <c:out value="${f.getMinWeight()}"/> </td>
                            <td> <c:out value="${f.getMaxWeight()}"/> </td>
                            <td> <c:out value="${f.getMinVolume()}"/> </td>
                            <td> <c:out value="${f.getMaxVolume()}"/> </td>
                            <td> <c:out value="${f.getVehicleBodyType().getName()}"/> </td>
                            <td> <c:out value="${f.getNumberOfAutos()}"/> </td>
                            <td> <c:out value="${f.getVehicleUsageAmount().getName()}"/> </td>
                            <td> <c:out value="${f.getPaymentMethods().getName()}"/> </td>
                            <td> <c:out value="${f.getPaymentMoments().getName()}"/> </td>
                            <td> <c:out value="${f.getPrice()}"/> </td>
                            <td> <c:out value="${f.getCurrency().getName()}"/> </td>
                            <td>per  <c:out value="${f.getCalculateUnit().getName()}"/> </td>
                            <td> <c:out value="${f.getPrepayment()}"/> </td>
                            <td> <c:out value="${f.getNotes()}"/> </td>
                            <td> <c:out value="${f.getUpdatedDate()}"/> </td>
                            <td> <c:out value="${f.getCreatedDate()}"/> </td>
                          </tr>
                        </c:forEach>
                        

                    </tbody>
                </table>
            </div>
        </div>
        
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
