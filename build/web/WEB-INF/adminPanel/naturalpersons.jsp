
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">

            <div class="portlet-body">
               
                <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                    <thead>
                        <tr>
                            <th> ID</th>
                            <th> Full Name </th>
                            <th> Username </th>
                           <th> Telephone 1 </th>
                           
                            <th> Telephone 2</th>
                            <th>Email</th>
                            <th> Country </th>
                            <th> City </th>
                            <th> Address </th>
                            <th> Date of registration</th>
                            <th> Date of last visit</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${personList}" var="p">
                      <tr>
                           <td> <c:out value="${p.getId()}"/> </td>
                           <td> <c:out value="${p.getFullName()}"/> </td>
                            <td> <c:out value="${p.getUserName()}"/></td>
                            <td> <c:out value="${p.getTelephone1()}"/> </td>
                            <td> <c:out value="${p.getTelephone2()}"/> </td>
                            <td class="center"> <c:out value="${p.getEmail()}"/> </td>
                            <td><c:out value="${p.getCountry().getName()}"/></td>
                            <td><c:out value="${p.getCity().getName()}"/></td>
                            <td><c:out value="${p.getAddress()}"/></td>
                            <td><c:out value="${p.getDateOfRegistration()}"/></td>
                            <td><c:out value="${p.getDateOfLastVisit()}"/></td>
                          
                        </tr>
                        </c:forEach>
                       </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
