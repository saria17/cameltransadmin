
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">

            <div class="portlet-body">
               
                <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                    <thead>
                        <tr>
                            <th> ID </th>
                            <th> Vehicle ID </th>
                            <th> Capacity </th>
                            <th> Load Capacity </th>
                            <th> Width </th>
                            <th> Length </th>
                            <th> Height </th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${trailerList}" var="p">
                            <tr>
                                <td>  <c:out value="${p.getId()}"/> </td>
                                <td>  <c:out value="${p.getVehicle().getId()}"/>  </td>
                                <td>  <c:out value="${p.getCapacity()}"/>  </td>
                                <td>  <c:out value="${p.getLoadCapacity()}"/>  </td>
                                <td>  <c:out value="${p.getWidth()}"/>  </td>
                                <td>  <c:out value="${p.getLength()}"/>  </td>
                                <td>  <c:out value="${p.getHeight()}"/>  </td>
                                
                            </tr>

                        </c:forEach>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
