
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">
           
            <div class="portlet-body">
               
                <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                    <thead>
                        <tr>
                            <th> Vehicle Ads ID </th>
                            <th> Vehicle  ID </th>
                            <th> From date </th>
                            <th> To date </th>
                            <th> From city </th>
                            <th> To city</th>
                            <th> From region</th>
                            <th> To region</th>
                            <th> Updated date</th>
                            <th> Edit </th>
                            <th> Delete </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${vehicleAdsList}" var="p">
                        <tr>
                            <td>${p.getId()}  </td>
                            <td>${p.getVehicle().getId()}  </td>
                            <td>${p.getFromDate()} </td>
                            <td class="center">${p.getToDate()}  </td>
                            <td> ${p.getFromCity().getName()} </td>
                            <td> ${p.getToCity().getName()} </td>
                            <td> ${p.getFromRegion().getName()}</td>
                            <td class="center">${p.getToRegion().getName()}  </td>
                             <td> ${p.getUpdatedDate()} </td>
                            
                            <td>
                                <a class="edit" href="javascript:;"> Edit </a>
                            </td>
                            <td>
                                <a class="delete" href="javascript:;"> Delete </a>
                            </td>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
