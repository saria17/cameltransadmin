
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">
           
            <div class="portlet-body">
              
                <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                    <thead>
                        <tr>
                            <th> Vehicle ID </th>
                            <th> User </th>
                            <th> ADR </th>
                            <th> Body type </th>
                            <th> Body length</th>
                            <th> Body height</th>
                            <th> Body width</th>
                            <th> Capacity</th>
                            <th> Load capacity</th>
                            <th> Trailer</th>
                            <th> Quantity</th>
                            <th> Price</th>
                            <th> Currency</th>
                            <th> Unit</th>
                            <th> Prepayment ratio</th>
                            <th> Payment method</th>
                            <th> Payment moment</th>
                            <th> Note</th>
                            <th> Created date</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${vehicleList}" var="p">
                        <tr>
                            <td>${p.getId()}  </td>
                            <td>${p.getUser().getFullName()}  </td>
                            <td>${p.getAdr().getNumber()} </td>
                            <td class="center">${p.getVehicleBodyType().getName()}  </td>
                            <td> ${p.getBodyLength()} </td>
                            <td> ${p.getBodyHeight()} </td>
                            <td> ${p.getBodyWidth()}</td>
                            <td class="center">${p.getCapacity()}  </td>
                            <td>  ${p.getLoadCapacity()}</td>
                            <td> ${p.getTrailer()} </td>
                            <td>${p.getQuantity()} </td>
                            <td>  ${p.getPrice()}</td>
                            <td> ${p.getCurrency().getName()} </td>
                            <td>${p.getCalculateUnit().getName()} </td>
                            <td class="center"> ${p.getPrepaymentRatio()} </td>
                            <td> ${p.getPaymentMethod().getName()} </td>
                            <td> ${p.getPaymentMoment().getName()} </td>
                            <td class="center"> ${p.getNotes()} </td>
                            <td> ${p.getCreatedDate()} </td>
                            
                         </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
