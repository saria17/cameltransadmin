$(document).ready(function () {
    $('.btn-close').click(function (e) {
        e.preventDefault();
        $(this).parent().parent().parent().fadeOut();
    });
    $('.btn-minimize').click(function (e) {
        e.preventDefault();
        var $target = $(this).parent().parent().next('.box-content');
        if ($target.is(':visible'))
            $('i', $(this)).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        else
            $('i', $(this)).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        $target.slideToggle();
    });
    $('.add-country').click(function (e) {
        e.preventDefault();
        $('#addcountry').modal('show');


    });
    $('.add-city').click(function (e) {
        e.preventDefault();
        $('#addcity').modal('show');


    });
    $('.add-region').click(function (e) {
        e.preventDefault();
        $('.modal-body #body_part').val($(this).data('id2'));
        $('#addregion').modal('show');


    });
    $('.add-adr').click(function (e) {
        e.preventDefault();
        $('#addadr').modal('show');


    });
    $('.add-money').click(function (e) {
        e.preventDefault();
        $('#addmoney').modal('show');


    });
    $('.add-pmethod').click(function (e) {
        e.preventDefault();
        $('#addpmethod').modal('show');


    });
    $('.add-pmoment').click(function (e) {
        e.preventDefault();
        $('#addpmoment').modal('show');


    });
    $('.add-unit').click(function (e) {
        e.preventDefault();
        $('#addunit').modal('show');


    });
    $('.add-loadingtype').click(function (e) {
        e.preventDefault();
        $('#addloadingtype').modal('show');


    });
    $('.add-bodytype').click(function (e) {
        e.preventDefault();
        $('#addbodytype').modal('show');


    });
    $('.add-categ').click(function (e) {
        e.preventDefault();
        $('#addcateg').modal('show');


    });
    $('.add-permission').click(function (e) {
        e.preventDefault();
        $('#addpermission').modal('show');


    });
    $('.add-amount').click(function (e) {
        e.preventDefault();
        $('#addamount').modal('show');


    });
    $('.add-admin').click(function (e) {
        e.preventDefault();
        $('#addadmin').modal('show');


    });
    $('.delete-row').click(function (e) {
        e.preventDefault();
        $('.modal-body #id').val($(this).data('id'));
        $('.modal-body #idvalue').empty();
        $('.modal-body #idvalue').append($(this).data('id'));
        $('#deleterow').modal('show');
       


    });
     $('.edit-admin').click(function (e) {
        e.preventDefault();
        $('.modal-body #id').val($(this).data('id'));
        $('.modal-body #name').val($(this).data('name'));
        $('#editadmin').modal('show');
     });
       $('.edit-row').click(function (e) {
        e.preventDefault();
        $('.edit #id').val($(this).data('id'));
        $('.edit #name').val($(this).data('name'));
        $('.edit #description').val($(this).data('desc'));
        $('.edit #country').val($(this).data('country'));
        $('.edit #region').val($(this).data('region'));
        $('.edit #code').val($(this).data('code'));
        $('.edit #bodycateg').val($(this).data('bodycateg'));
        $('#editrow').modal('show');
     });
   
   
   
});