
package com.cameltrans.admin.controller;

import com.cameltrans.crosscutting.models.AdminModel;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author Sariya
 */
public class AdminAuthentication extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();
        if (!uri.endsWith("login") && !uri.endsWith("logout") && !uri.endsWith("panel")) {
            AdminModel model = (AdminModel) request.getSession().getAttribute("LOGGEDIN_USER");
            if (model == null) {
                response.sendRedirect("/CamelTransAdmin/admin/login");
                return false;
            }

        }
        return true;
    }

}
