package com.cameltrans.admin.controller;

import com.cameltrans.crosscutting.models.ADRModel;
import com.cameltrans.crosscutting.models.AdminModel;
import com.cameltrans.crosscutting.models.CityModel;
import com.cameltrans.crosscutting.models.CountryModel;
import com.cameltrans.crosscutting.models.LoadingTypeModel;
import com.cameltrans.crosscutting.models.MoneyModel;
import com.cameltrans.crosscutting.models.PaymentMethodModel;
import com.cameltrans.crosscutting.models.PaymentMomentModel;
import com.cameltrans.crosscutting.models.PermissionModel;
import com.cameltrans.crosscutting.models.RegionModel;
import com.cameltrans.crosscutting.models.UnitModel;
import com.cameltrans.crosscutting.models.VehicleBodyCategoryModel;
import com.cameltrans.crosscutting.models.VehicleBodyTypeModel;
import com.cameltrans.crosscutting.models.VehicleUsageAmountModel;
import com.cameltrans.dal.repositories.ADRRepository;
import com.cameltrans.dal.repositories.AdminRepository;
import com.cameltrans.dal.repositories.CityRepository;
import com.cameltrans.dal.repositories.CountryRepository;
import com.cameltrans.dal.repositories.FreightRepository;
import com.cameltrans.dal.repositories.LegalEntityRepository;
import com.cameltrans.dal.repositories.LoadingTypeRepository;
import com.cameltrans.dal.repositories.MoneyRepository;
import com.cameltrans.dal.repositories.NaturalPersonRepository;
import com.cameltrans.dal.repositories.PaymentMethodRepository;
import com.cameltrans.dal.repositories.PaymentMomentRepository;
import com.cameltrans.dal.repositories.PermissionRepository;
import com.cameltrans.dal.repositories.RegionRepository;
import com.cameltrans.dal.repositories.TrailerRepository;
import com.cameltrans.dal.repositories.UnitRepository;
import com.cameltrans.dal.repositories.UserRepository;
import com.cameltrans.dal.repositories.VehicleAdvertisementRepository;
import com.cameltrans.dal.repositories.VehicleBodyCategoryRepository;
import com.cameltrans.dal.repositories.VehicleBodyTypeRepository;
import com.cameltrans.dal.repositories.VehicleRepository;
import com.cameltrans.dal.repositories.VehicleUsageAmountRepository;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Sariya
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    AdminRepository admin;
    @Autowired
    AdminRepository repo;
    @Autowired
    ADRRepository adr;
    @Autowired
    CountryRepository country;
    @Autowired
    CityRepository city;
    @Autowired
    RegionRepository region;
    @Autowired
    PermissionRepository per;
    @Autowired
    MoneyRepository money;
    @Autowired
    PaymentMethodRepository pay;
    @Autowired
    PaymentMomentRepository paym;
    @Autowired
    LoadingTypeRepository type;
    @Autowired
    UnitRepository unit;
    @Autowired
    VehicleBodyCategoryRepository categ;
    @Autowired
    VehicleUsageAmountRepository amount;
    @Autowired
    NaturalPersonRepository nuser;
    @Autowired
    LegalEntityRepository legal;
    @Autowired
    VehicleBodyTypeRepository btype;
    @Autowired
    UserRepository user;
    @Autowired
    VehicleRepository vehicle;
    @Autowired
    VehicleAdvertisementRepository ads;
    @Autowired
    TrailerRepository trailer;
    @Autowired
    FreightRepository freight;

    /**
     * return login form
     *
     * @param req
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView adminLogin(HttpServletRequest req) {
        if (req.getSession().getAttribute("LOGGEDIN_USER") != null) {
            return new ModelAndView("adminpanel");
        }
        return new ModelAndView("adminlogin");
    }

    /**
     * logout invalidate session if not null
     *
     * @param req
     * @return
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView adminLogout(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        return new ModelAndView("redirect:/admin/login");
    }

    /**
     * 1.check form if has errors return error page 2.check user name, password
     * if exists login admin, create session, else return error page
     *
     * @param admin
     * @param result
     * @param status
     * @param request
     * @return
     */
    @RequestMapping(value = "/panel", method = {RequestMethod.POST})
    public ModelAndView adminPanel(@Valid @ModelAttribute("admin") AdminModel admin,
            BindingResult result, SessionStatus status, HttpServletRequest request, HttpServletResponse resp) {
        String view = "adminlogin";
        ModelAndView modelview = new ModelAndView(view);
        if (result.hasErrors()) {
            return modelview;
        }

        int res = repo.Login(admin);
        if (res == 0) {
            modelview.getModel().put("errormessage", "Invalid username or password");
        } else {

            view = "redirect:/admin/home";
            HttpSession session = request.getSession();
            session.setAttribute("LOGGEDIN_USER", admin);
            if (request.getParameter("mark") != null && request.getParameter("mark").equals("true")) {
            }
            status.setComplete();
        }
        modelview.setViewName(view);

        return modelview;
    }

    @RequestMapping(value = "/panel", method = {RequestMethod.GET})
    public String getPanel() {
        return "redirect:/admin/login";
    }
    /*
     return home page
     */

    @RequestMapping(value = {"/home"}, method = RequestMethod.GET)
    public ModelAndView adminHome() {
        return new ModelAndView("adminpanel");
    }
    /*
     Return Tables with corresponding data
     */

    @RequestMapping(value = "/Admins", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView admins() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "admins.jsp");
        model.addObject("title", "Admins");
        model.addObject("adminList", admin.getAll().getObjects());
        model.addObject("admin", new AdminModel());
        return model;
    }

    @RequestMapping(value = "/Users", method = RequestMethod.GET)
    public ModelAndView users() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "users.jsp");
        model.addObject("title", "Users");
        return model;
    }

    @RequestMapping(value = "/LegalEntities", method = RequestMethod.GET)
    public ModelAndView editabletable() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "legalentities.jsp");
        model.addObject("title", "Legal Entities");
        model.addObject("personList", legal.getAll().getObjects());
        return model;

    }

    @RequestMapping(value = "/NaturalPersons", method = RequestMethod.GET)
    public ModelAndView naturalpersons() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "naturalpersons.jsp");
        model.addObject("title", "Natural Persons");
        model.addObject("personList", nuser.getAll().getObjects());

        return model;

    }

    @RequestMapping(value = "/Freights", method = RequestMethod.GET)
    public ModelAndView freights() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "freights.jsp");
        model.addObject("title", "Freights");
        model.addObject("freightList", freight.getAll().getObjects());
        return model;

    }

    @RequestMapping(value = "/Vehicles", method = RequestMethod.GET)
    public ModelAndView vehicles() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "vehicles.jsp");
        model.addObject("title", "Vehicles");
        model.addObject("vehicleList", vehicle.getAll().getObjects());
        return model;

    }

    @RequestMapping(value = "/Trailers", method = RequestMethod.GET)
    public ModelAndView trailers() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "trailers.jsp");
        model.addObject("title", "Trailers");
        model.addObject("trailerList", trailer.getAll().getObjects());

        return model;

    }

    @RequestMapping(value = "/VehicleAds", method = RequestMethod.GET)
    public ModelAndView vehicleAds() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "vehicleAds.jsp");
        model.addObject("title", "Vehicle ADS");
        model.addObject("vehicleAdsList", ads.getAll().getObjects());

        return model;

    }

    @RequestMapping(value = "/Countries", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView countries() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "countries.jsp");
        model.addObject("country", new CountryModel());
        model.addObject("title", "Countries");
        model.addObject("countryList", country.getAll().getObjects());
        return model;

    }

    @RequestMapping(value = "/Cities", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView cities() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "cities.jsp");
        model.addObject("title", "Cities");
        model.addObject("countries", country.getAll().getObjects());
        model.addObject("cityList", city.getAll().getObjects());
        return model;

    }

    @RequestMapping(value = "/Regions", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView regions() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "regions.jsp");
        model.addObject("title", "Regions");
        model.addObject("regionList", region.getAll().getObjects());
        model.addObject("countries", country.getAll().getObjects());
        return model;

    }

    @RequestMapping(value = "/ADR", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView adrs() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "adr.jsp");
        model.addObject("title", "ADR");
        model.addObject("adrList", adr.getAll().getObjects());
        return model;
    }

    @RequestMapping(value = "/Permissions", method = {RequestMethod.GET,RequestMethod.POST})
    public ModelAndView permissions() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "permissions.jsp");
        model.addObject("title", "Permissions");
        model.addObject("permissionList", per.getAll().getObjects());
        return model;
    }

    @RequestMapping(value = "/Moneys", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView moneys() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "moneys.jsp");
        model.addObject("title", "Moneys");
        model.addObject("countries", country.getAll().getObjects());
        model.addObject("moneyList", money.getAll().getObjects());
        return model;
    }

    @RequestMapping(value = "/PaymentMethods", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView paymentmethods() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "paymentmethods.jsp");
        model.addObject("title", "Payment Methods");
        model.addObject("paymentMethodList", pay.getAll().getObjects());
        return model;
    }

    @RequestMapping(value = "/PaymentMoments", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView paymentmoments() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "paymentmoments.jsp");
        model.addObject("title", "Payment Moments");
        model.addObject("paymentMomentList", paym.getAll().getObjects());
        return model;
    }

    @RequestMapping(value = "/LoadingTypes", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView loadingtypes() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "loadingtypes.jsp");
        model.addObject("title", "Loading Types");
        model.addObject("loadingTypeList", type.getAll().getObjects());
        return model;
    }

    @RequestMapping(value = "/Units", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView units() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "units.jsp");
        model.addObject("title", "Units");
        model.addObject("unitList", unit.getAll().getObjects());
        return model;
    }

    @RequestMapping(value = "/VehicleBodyCategories", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView bodycategories() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "bodycategories.jsp");
        model.addObject("title", "Vehicle Body Categories");
        model.addObject("categoryList", categ.getAll().getObjects());
        return model;
    }

    @RequestMapping(value = "/VehicleBodyTypes", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView bodytypes() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "bodytypes.jsp");
        model.addObject("categs", categ.getAll().getObjects());
        model.addObject("title", "Vehicle Body Types");
        model.addObject("typeList", btype.getAll().getObjects());
        return model;
    }

    @RequestMapping(value = "/VehicleUsageAmounts", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView usageamounts() {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "usageamounts.jsp");
        model.addObject("title", "Vehicle Usage Amounts");
        model.addObject("amountList", amount.getAll().getObjects());
        return model;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(Integer.class, new Editor(Integer.class, true));
        binder.registerCustomEditor(Double.class, new Editor(Double.class, true));

    }

    /**
     * 1.check errors before adding 2.check for country name, if exists set
     * error message 3.add country set success message 4.if not added set error
     * message
     *
     * @param countrym
     * @param res
     * @return
     */
    @RequestMapping(value = "/AddCountry", method = RequestMethod.POST)
    public ModelAndView addcountry(@Valid @ModelAttribute("country") CountryModel countrym, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/Countries");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding country.";

        } else {
            if (!country.checkCountryExistence(countrym)) {
                int i = country.add(countrym);
                if (i != 0) {
                    msg = "successfully added";
                    name = "smsg";
                    model.setViewName("redirect:/admin/Countries");
                } else {
                    msg = "Unexpected error occurred, while adding country.";
                }
            } else {
                msg = " Duplicate entry for country \"" + countrym.getName() + "\"";
            }
        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddCountry", "/DeleteCountry"}, method = RequestMethod.GET)
    public ModelAndView addCountryget() {
        ModelAndView model = new ModelAndView("redirect:/admin/Countries");
        return model;
    }

    @RequestMapping(value = "/DeleteCountry", method = RequestMethod.POST)
    public String deletecity(@ModelAttribute CountryModel countrym, Model model) {
        String view = "redirect:/admin/Countries";
        int i = country.delete(countrym);
        if (i != 1) {
            view = "forward:/admin/Countries";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/AddCity", method = RequestMethod.POST)
    public ModelAndView addcity(@Valid @ModelAttribute("city") CityModel citym, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/Cities");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding city.";

        } else {
            if (!city.checkCityExistence(citym)) {
                int i = city.add(citym);
                if (i != 0) {
                    msg = "successfully added";
                    name = "smsg";
                    model.setViewName("redirect:/admin/Cities");
                } else {
                    msg = "Unexpected error occurred, while adding city.";
                }
            } else {
                msg = " Duplicate entry for city \"" + citym.getName() + "\"";
            }
        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddCity", "/DeleteCity"}, method = RequestMethod.GET)
    public ModelAndView addCityget() {
        ModelAndView model = new ModelAndView("redirect:/admin/Cities");
        return model;
    }

    @RequestMapping(value = "/DeleteCity", method = RequestMethod.POST)
    public String deletecity(@ModelAttribute CityModel citym, Model model) {
        String view = "redirect:/admin/Cities";
        int i = city.delete(citym);
        if (i != 1) {
            view = "forward:/admin/Cities";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/AddRegion", method = RequestMethod.POST)
    public ModelAndView addregion(@Valid @ModelAttribute("region") RegionModel regionm, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/Regions");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding region.";

        } else {
            int i = region.add(regionm);
            if (i != 0) {
                msg = "successfully added";
                name = "smsg";
                model.setViewName("redirect:/admin/Regions");
            } else {
                msg = "Unexpected error occurred, while adding region.";
            }

        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddRegion", "/DeleteRegion"}, method = RequestMethod.GET)
    public ModelAndView addRegionget() {
        ModelAndView model = new ModelAndView("redirect:/admin/Regions");
        return model;
    }

    @RequestMapping(value = "/DeleteRegion", method = RequestMethod.POST)
    public String deleteregion(@ModelAttribute RegionModel regionm, Model model) {
        String view = "redirect:/admin/Regions";
        int i = region.delete(regionm);
        if (i != 1) {
            view = "forward:/admin/Regions";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/AddCurrency", method = RequestMethod.POST)
    public ModelAndView addmoney(@Valid @ModelAttribute("money") MoneyModel moneym, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/Moneys");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding money.";

        } else {
            int i = money.add(moneym);
            if (i != 0) {
                msg = "successfully added";
                name = "smsg";
                model.setViewName("redirect:/admin/Moneys");
            } else {
                msg = "Unexpected error occurred, while adding money.";
            }

        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddCurrency", "/DeleteCurrency", "/EditCurrency"}, method = RequestMethod.GET)
    public ModelAndView addMoneyget() {
        ModelAndView model = new ModelAndView("redirect:/admin/Moneys");
        return model;
    }

    @RequestMapping(value = "/DeleteCurrency", method = RequestMethod.POST)
    public String deletemoney(@ModelAttribute MoneyModel moneym, Model model) {
        String view = "redirect:/admin/Moneys";
        int i = money.delete(moneym);
        if (i != 1) {
            view = "forward:/admin/Moneys";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/AddPaymentMethod", method = RequestMethod.POST)
    public ModelAndView addpmethod(@Valid @ModelAttribute("pmethod") PaymentMethodModel pmethodm, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/PaymentMethods");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding a payment method.";

        } else {
            if (!pay.checkPaymentMethodExistence(pmethodm)) {
                int i = pay.add(pmethodm);
                if (i != 0) {
                    msg = "successfully added";
                    name = "smsg";
                    model.setViewName("redirect:/admin/PaymentMethods");
                } else {
                    msg = "Unexpected error occurred, while adding a payment method.";
                }
            } else {
                msg = " Duplicate entry for payment method \"" + pmethodm.getName() + "\"";
            }
        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddPaymentMethod", "/DeletePaymentMethod"}, method = RequestMethod.GET)
    public ModelAndView addpmethodget() {
        ModelAndView model = new ModelAndView("redirect:/admin/PaymentMethods");
        return model;
    }

    @RequestMapping(value = "/DeletePaymentMethod", method = RequestMethod.POST)
    public String deletepmethod(@ModelAttribute("pmethod") PaymentMethodModel pmethodm, Model model) {
        String view = "redirect:/admin/PaymentMethods";
        int i = pay.delete(pmethodm);
        if (i != 1) {
            view = "forward:/admin/PaymentMethods";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/AddPaymentMoment", method = RequestMethod.POST)
    public ModelAndView addpmoment(@Valid @ModelAttribute PaymentMomentModel pmomentm, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/PaymentMoments");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding a payment moment.";

        } else {
            if (!paym.checkPaymentMomentExistence(pmomentm)) {
                int i = paym.add(pmomentm);
                if (i != 0) {
                    msg = "successfully added";
                    name = "smsg";
                    model.setViewName("redirect:/admin/PaymentMoments");
                } else {
                    msg = "Unexpected error occurred, while adding a payment moment.";
                }
            } else {
                msg = " Duplicate entry for payment moment \"" + pmomentm.getName() + "\"";
            }
        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddPaymentMoment", "/AddPaymentMoment"}, method = RequestMethod.GET)
    public ModelAndView addpmomentget() {
        ModelAndView model = new ModelAndView("redirect:/admin/PaymentMoments");
        return model;
    }

    @RequestMapping(value = "/DeletePaymentMoment", method = RequestMethod.POST)
    public String deletepmoment(@ModelAttribute PaymentMomentModel pmomentm, Model model) {
        String view = "redirect:/admin/PaymentMoments";
        int i = paym.delete(pmomentm);
        if (i != 1) {
            view = "forward:/admin/PaymentMoments";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/AddUnit", method = RequestMethod.POST)
    public ModelAndView addunit(@Valid @ModelAttribute("unit") UnitModel unitm, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/Units");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding a unit.";

        } else {
            if (!unit.checkUnitExistence(unitm)) {
                int i = unit.add(unitm);
                if (i != 0) {
                    msg = "successfully added";
                    name = "smsg";
                    model.setViewName("redirect:/admin/Units");
                } else {
                    msg = "Unexpected error occurred, while adding a unit.";
                }
            } else {
                msg = " Duplicate entry for unit \"" + unitm.getName() + "\"";
            }
        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddUnit", "/DeleteUnit"}, method = RequestMethod.GET)
    public ModelAndView addunitget() {
        ModelAndView model = new ModelAndView("redirect:/admin/Units");
        return model;
    }

    @RequestMapping(value = "/DeleteUnit", method = RequestMethod.POST)
    public String deleteunit(@ModelAttribute("unit") UnitModel unitm, Model model) {
        String view = "redirect:/admin/Units";
        int i = unit.delete(unitm);
        if (i != 1) {
            view = "forward:/admin/Units";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/AddPermission", method = RequestMethod.POST)
    public ModelAndView addpermission(@Valid @ModelAttribute("permission") PermissionModel perm, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/Permissions");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding a permission.";

        } else {
            if (!per.checkPermissionExistence(perm)) {
                int i = per.add(perm);
                if (i != 0) {
                    msg = "successfully added";
                    name = "smsg";
                    model.setViewName("redirect:/admin/Permissions");
                } else {
                    msg = "Unexpected error occurred, while adding a permission.";
                }
            } else {
                msg = " Duplicate entry for permission \"" + perm.getName() + "\"";
            }
        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddPermission", "/DeletePermission"}, method = RequestMethod.GET)
    public ModelAndView addpermissionget() {
        ModelAndView model = new ModelAndView("redirect:/admin/Permissions");
        return model;
    }

    @RequestMapping(value = "/DeletePermission", method = RequestMethod.POST)
    public String deleteper(@ModelAttribute("permission") PermissionModel perm, Model model) {
        String view = "redirect:/admin/Permissions";
        int i = per.delete(perm);
        if (i != 1) {
            view = "forward:/admin/Permissions";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/AddADR", method = RequestMethod.POST)
    public ModelAndView addadr(@Valid @ModelAttribute("adr") ADRModel adrm, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/ADR");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding an ADR.";

        } else {

            int i = adr.add(adrm);
            if (i != 0) {
                msg = "successfully added";
                name = "smsg";
                model.setViewName("redirect:/admin/ADR");
            } else {
                msg = "Unexpected error occurred, while adding an ADR.";
            }

        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddADR", "/DeleteADR"}, method = RequestMethod.GET)
    public ModelAndView addadrget() {
        ModelAndView model = new ModelAndView("redirect:/admin/ADR");
        return model;
    }

    @RequestMapping(value = "/DeleteADR", method = RequestMethod.POST)
    public String deleteadr(@ModelAttribute("adr") ADRModel adrm, Model model) {
        String view = "redirect:/admin/ADR";
        int i = adr.delete(adrm);
        if (i != 1) {
            view = "forward:/admin/ADR";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    /**
     * 1.check errors before adding 2.check for username, if exists set error
     * message ,else set add.
     *
     * @param adminm
     * @param res
     * @return model
     */
    @RequestMapping(value = "/RegisterAdmin", method = RequestMethod.POST)
    public ModelAndView registeradmin(@Valid @ModelAttribute("admin") AdminModel adminm, BindingResult res) {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "admins.jsp");
        model.addObject("title", "Admins");
        model.addObject("adminList", admin.getAll().getObjects());
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while registering a new admin.";

        } else if (!adminm.getPassword().equals(adminm.getRpassword())) {
            msg = "Please, enter the same password again.";
        } else {
            if (!admin.checkAdminExistence(adminm)) {
                int i = admin.add(adminm);
                if (i != 0) {
                    msg = "successfully added";
                    name = "smsg";
                    model.setViewName("redirect:/admin/Admins");
                } else {
                    msg = "Unexpected error occurred while adding a new admins.";
                }
            } else {
                msg = " Duplicate entry for username \"" + adminm.getName() + "\"";
            }
        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/RegisterAdmin", "/DeleteAdmin", "/EditAdmin"}, method = RequestMethod.GET)
    public String adminget() {
        String model = "redirect:/admin/Admins";
        return model;
    }

    @RequestMapping(value = "/DeleteAdmin", method = RequestMethod.POST)
    public String deleteAdmin(@ModelAttribute("admin") AdminModel adminm, Model model) {
        String view = "redirect:/admin/Admins";
        int i = admin.delete(adminm);
        if (i != 1) {
            view = "forward:/admin/Admins";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/EditAdmin", method = RequestMethod.POST)
    public ModelAndView editadmin(@Valid @ModelAttribute("admin") AdminModel adminm, BindingResult res) {
        ModelAndView model = new ModelAndView("adminpanel");
        model.addObject("partial", "admins.jsp");
        model.addObject("title", "Admins");
        model.addObject("adminList", admin.getAll().getObjects());
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while updating the row.";

        } else if (!adminm.getPassword().equals(adminm.getRpassword())) {
            msg = "Please, enter the same password again.";
        } else {

            int i = admin.update(adminm);
            if (i != 0) {
                msg = "successfully updated";
                name = "smsg";
                model.setViewName("redirect:/admin/Admins");
            } else {
                msg = "Unexpected error occurred while updating the row.";
            }

        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = "/AddBodyCategory", method = RequestMethod.POST)
    public ModelAndView addBCateg(@Valid @ModelAttribute VehicleBodyCategoryModel bcateg, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/VehicleBodyCategories");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding a new category.";

        } else {
            if (!categ.checkCategExistence(bcateg)) {
                int i = categ.add(bcateg);
                if (i != 0) {
                    msg = "successfully added";
                    name = "smsg";
                    model.setViewName("redirect:/admin/VehicleBodyCategories");
                } else {
                    msg = "Unexpected error occurred while adding a new category.";
                }
            } else {
                msg = " Duplicate entry for category \"" + bcateg.getName() + "\"";
            }
        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddBodyCategory", "/DeleteBodyCategory"}, method = RequestMethod.GET)
    public String bcategGet() {
        String model = "redirect:/admin/VehicleBodyCategories";
        return model;
    }

    @RequestMapping(value = "/DeleteBodyCategory", method = RequestMethod.POST)
    public String deleteBcateg(@ModelAttribute("bcateg") VehicleBodyCategoryModel bcateg, Model model) {
        String view = "redirect:/admin/VehicleBodyCategories";
        int i = categ.delete(bcateg);
        if (i != 1) {
            view = "forward:/admin/VehicleBodyCategories";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/AddBodyType", method = RequestMethod.POST)
    public ModelAndView addBType(@Valid @ModelAttribute VehicleBodyTypeModel btypem, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/VehicleBodyTypes");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding a new body type.";

        } else {
            if (!btype.checkBtypeExistence(btypem)) {
                int i = btype.add(btypem);
                if (i != 0) {
                    msg = "successfully added";
                    name = "smsg";
                    model.setViewName("redirect:/admin/VehicleBodyTypes");
                } else {
                    msg = "Unexpected error occurred while adding a new body type.";
                }
            } else {
                msg = " Duplicate entry for body type \"" + btypem.getName() + "\"";
            }
        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddBodyType", "/DeleteBodyType"}, method = RequestMethod.GET)
    public String btypeGet() {
        String model = "redirect:/admin/VehicleBodyTypes";
        return model;
    }

    @RequestMapping(value = "/DeleteBodyType", method = RequestMethod.POST)
    public String deleteBtype(@ModelAttribute VehicleBodyTypeModel btypem, Model model) {
        String view = "redirect:/admin/VehicleBodyTypes";
        int i = btype.delete(btypem);
        if (i != 1) {
            view = "forward:/admin/VehicleBodyTypes";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/AddLoadingType", method = RequestMethod.POST)
    public ModelAndView addLType(@Valid @ModelAttribute LoadingTypeModel ltypem, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/LoadingTypes");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding a new loading type.";

        } else {
            if (!type.checkLTypeExistence(ltypem)) {
                int i = type.add(ltypem);
                if (i != 0) {
                    msg = "successfully added";
                    name = "smsg";
                    model.setViewName("redirect:/admin/LoadingTypes");
                } else {
                    msg = "Unexpected error occurred while adding a new loading type.";
                }
            } else {
                msg = " Duplicate entry for loading type \"" + ltypem.getName() + "\"";
            }
        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddLoadingType", "/DeleteLoadingType"}, method = RequestMethod.GET)
    public String ltypeGet() {
        String model = "redirect:/admin/LoadingTypes";
        return model;
    }

    @RequestMapping(value = "/DeleteLoadingType", method = RequestMethod.POST)
    public String deleteLtype(@ModelAttribute LoadingTypeModel ltypem, Model model) {
        String view = "redirect:/admin/VehicleLoadingTypes";
        int i = type.delete(ltypem);
        if (i != 1) {
            view = "forward:/admin/VehicleLoadingTypes";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    @RequestMapping(value = "/AddUsageAmount", method = RequestMethod.POST)
    public ModelAndView addamount(@Valid @ModelAttribute VehicleUsageAmountModel amountm, BindingResult res) {
        ModelAndView model = new ModelAndView("forward:/admin/VehicleUsageAmounts");
        String msg = "";
        String name = "msg";
        if (res.hasErrors()) {
            System.out.println(res.getAllErrors());
            msg = "Error while adding a new usage amount.";

        } else {
            if (!amount.checkAmountExistence(amountm)) {
                int i = amount.add(amountm);
                if (i != 0) {
                    msg = "successfully added";
                    name = "smsg";
                    model.setViewName("redirect:/admin/VehicleUsageAmounts");
                } else {
                    msg = "Unexpected error occurred while adding a new usage amount.";
                }
            } else {
                msg = " Duplicate entry for usage amount \"" + amountm.getName() + "\"";
            }
        }
        model.addObject(name, msg);
        return model;
    }

    @RequestMapping(value = {"/AddUsageAmount", "/DeleteUsageAmount"}, method = RequestMethod.GET)
    public String amountGet() {
        String model = "redirect:/admin/VehicleUsageAmounts";
        return model;
    }

    @RequestMapping(value = "/DeleteUsageAmount", method = RequestMethod.POST)
    public String deleteUamount(@ModelAttribute VehicleUsageAmountModel amountm, Model model) {
        String view = "redirect:/admin/VehicleUsageAmounts";
        int i = amount.delete(amountm);
        if (i != 1) {
            view = "forward:/admin/VehicleUsageAmounts";
            model.addAttribute("msg", "Error while deleting the row.");
        }
        return view;
    }

    public String test(String a) {
        return a;
    }

}
