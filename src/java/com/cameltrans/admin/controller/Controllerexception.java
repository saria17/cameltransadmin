

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cameltrans.admin.controller;

import com.mysql.jdbc.CommunicationsException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class Controllerexception {

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ModelAndView handleError405(HttpServletRequest request, Exception e) {
        ModelAndView mav = new ModelAndView("errorpage");
        String msg = "Method not supported for this url: " + request.getRequestURL();
        mav.addObject("errormsg", msg);
        mav.addObject("errorcode", "405");
        return mav;
    }

    
     @ExceptionHandler(Exception.class)
     public ModelAndView handleError(HttpServletRequest request, Exception e) {
     ModelAndView mav = new ModelAndView("errorpage");
      
     String msg = e.toString();
     mav.addObject("errormsg", msg);
     mav.addObject("errorcode", "500");
     return mav;
     }
     
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView handleError404(HttpServletRequest request, Exception e) {
        ModelAndView mav = new ModelAndView("errorpage");

        String msg = "It seems you are lost. The page you are requested does not exist";
        mav.addObject("errormsg", msg);
        mav.addObject("errorcode", "404");
        return mav;
    }
    @ExceptionHandler({CannotGetJdbcConnectionException.class, DataAccessResourceFailureException.class, MetaDataAccessException.class, CommunicationsException.class})
    public ModelAndView handleConnectionerrpr(HttpServletRequest request, Exception e) {
        ModelAndView mav = new ModelAndView("errorpage");
      
        String msg = "Cannot connect to Server. Connection lost.";
        mav.addObject("errormsg", msg);
        mav.addObject("errorcode", "500");
        return mav;
    }

}
