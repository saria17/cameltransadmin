
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">

            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a data-toggle="model" class="btn btn-info add-country" href="" > Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <span style="color: red"> ${msg}</span>
                <span style="color: #006400"> ${smsg}</span>

                <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                    <thead>
                        <tr>
                            <th> ID </th>
                            <th> Name </th>
                            <th> Region </th>
                            <th> Code </th>
                            <th> Edit </th>
                            <th> Delete </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${countryList}" var="country">
                            <tr>
                                <td>  <c:out value="${country.getId()}"/> </td>
                                <td>  <c:out value="${country.getName()}"/>  </td>
                                <td>  <c:out value="${country.getRegion()}"/>  </td>
                                <td>  <c:out value="${country.getCode()}"/>   </td>
                                <td>
                                    <a data-toggle="model" class="btn btn-info edit-row" href="" data-id="${country.getId()}"  data-name="${country.getName()}" data-region="${country.getRegion()}" data-code="${country.getCode()}">
                                        <i class="glyphicon glyphicon-edit icon-white"></i>
                                        Edit
                                    </a> </td>
                                <td>
                                    <a data-toggle="model" class="btn btn-danger delete-row" data-id="${country.getId()}"> Delete
                                        <i class="fa fa-minus"></i>
                                    </a>
                                </td>
                            </tr>

                        </c:forEach>

                    </tbody>
                </table>
                <div class="modal fade" id="addcountry" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Adding</h3>
                        </div>
                        <div class="modal-body">
                            <f:form action="AddCountry" commandName="country" method="post" class="form1">
                                <div class="form-group">
                                    <label class="control-label"  for="name">Country Name</label>
                                    <f:input type="text" path="name" class="form-control"/> 
                                    <f:errors path="name"/>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"  for="region">Region</label>
                                    <f:input type="number" path="region" class="form-control"/>
                                    <f:errors path="region"/>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="code">Code</label>
                                    <f:input type="number" path="code"  class="form-control"/>
                                    <f:errors path="code"/>
                                </div>

                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Add"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>
                                </f:form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="deleterow" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Delete</h3>
                        </div>
                        <div class="modal-body">
                            <div>    <p>Are you sure to permanently delete row <b id="idvalue"></b> ?</p> </div>
                            <form action="DeleteCountry" method="post" >
                                <input type="hidden" name="Id" id="id" class="form-control"/>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Delete"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="editrow" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Editing</h3>
                        </div>
                        <div class="modal-body edit">
                            <f:form action="EditCountry" commandName="country" method="post" class="form1">
                                <input type="hidden" id="id" name="Id">
                                <div class="form-group">
                                    <label class="control-label"  for="name">Country Name</label>
                                    <f:input type="text" path="name" id="name" class="form-control"/> 
                                    <f:errors path="name"/>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"  for="region">Region</label>
                                    <f:input type="number" path="region" id="region" class="form-control"/>
                                    <f:errors path="region"/>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="code">Code</label>
                                    <f:input type="number" path="code" id="code" class="form-control"/>
                                    <f:errors path="code"/>
                                </div>

                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Add"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>
                                </f:form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>