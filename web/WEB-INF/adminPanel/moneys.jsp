   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">

            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a data-toggle="model" class="btn btn-info add-money"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <span style="color: red"> ${msg}</span>
                <span style="color: #006400"> ${smsg}</span>
                <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                    <thead>
                        <tr>
                            <th> ID </th>
                            <th> Name </th>
                            <th> Country</th>

                            <th> Edit </th>
                            <th> Delete </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${moneyList}" var="per">
                            <tr>
                                <td> <c:out value="${per.getId()}"/> </td>
                                <td><c:out value="${per.getName()}"/>  </td>

                                <td class="center"><c:out value="${per.getCountry().getName()}"/> </td>
                                <td>
                                    <a data-toggle="model" class="btn btn-info edit-row" href="" data-id="${per.getId()}"  data-name="${per.getName()}" data-country="${per.getCountry().getName()}">
                                        <i class="glyphicon glyphicon-edit icon-white"></i>
                                        Edit
                                    </a>
                                </td>
                                <td>
                                    <a data-toggle="model" class="btn btn-danger delete-row" data-id="${per.getId()}"> Delete
                                        <i class="fa fa-minus"></i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="modal fade" id="addmoney" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Adding</h3>
                        </div>
                        <div class="modal-body ">
                            <form action="AddCurrency" method="post" class="form1">
                                <div class="form-group">
                                    <label class="control-label"  for="name">Money Name</label>
                                    <input type="text" name="name" class="form-control"/> </div>

                                <div class="form-group">
                                    <label class="control-label" for="code">Country</label>
                                    <select class="form-control" name="country.Id">
                                        <c:forEach items="${countries}" var="c">

                                            <option value="${c.getId()}"><c:out value="${c.getName()}"/></option>

                                        </c:forEach>
                                    </select></div>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Add"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="deleterow" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Delete</h3>
                        </div>
                        <div class="modal-body">
                            <div>    <p>Are you sure to permanently delete row <b id="idvalue"></b> ?</p> </div>
                            <form action="DeleteCurrency" method="post" >
                                <input type="hidden" name="Id" id="id" class="form-control"/>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Delete"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="editrow" aria-hidden="true" > 
                    <div class="modal-dialog" style="background-color:white !important;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">�</button>
                            <h3>Editing</h3>
                        </div>
                        <div class="modal-body edit">
                            <form action="EditCurrency" method="post" class="form1">
                                <input type="hidden" id="id" name="Id">
                                <div class="form-group">
                                    <label class="control-label"  for="name">Money Name</label>
                                    <input type="text" name="name" id="name" class="form-control"/> </div>

                                <div class="form-group">
                                    <label class="control-label" for="code">Country</label>
                                    <select class="form-control" name="country.Id" id="country">
                                        <c:forEach items="${countries}" var="c">

                                            <option value="${c.getId()}"><c:out value="${c.getName()}"/></option>

                                        </c:forEach>
                                    </select></div>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Save"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a> </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
